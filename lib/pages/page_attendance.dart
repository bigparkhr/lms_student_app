import 'package:flutter/material.dart';
import 'package:getwidget/components/dropdown/gf_dropdown.dart';

class PageAttendance extends StatefulWidget {
  const PageAttendance({super.key});

  @override
  State<PageAttendance> createState() => _PageAttendanceState();
}

class _PageAttendanceState extends State<PageAttendance> {
  List<String> _data = []; // 데이터 리스트
  ScrollController _scrollController = ScrollController();

  String? yearDropdownValue = '2024'; // 기본적으로 2024을 선택한 상태로 초기화
  String? monthDropdownValue = '03'; // 기본적으로 03을 선택한 상태로 초기화

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _fetchMoreData(); // 스크롤이 끝에 도달했을 때 데이터를 불러오는 함수 호출
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose(); // 위젯이 dispose될 때 스크롤 컨트롤러도 함께 dispose
    super.dispose();
  }

  void _fetchMoreData() async {
    // 비동기 작업을 수행하여 새로운 데이터 가져오기 (예: 네트워크 요청)
    await Future.delayed(Duration(seconds: 2)); // 예시로 2초 동안 대기
    List<String> newData =
        List.generate(10, (index) => 'Item ${_data.length + index}');

    setState(() {
      _data.addAll(newData); // 가져온 데이터를 기존 데이터 리스트에 추가
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Padding(
            padding: EdgeInsets.only(top: 3),
            child: Text(
              '출결 현황',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
        ),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 7),
                    Text('훈련과정'),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              SizedBox(height: 5),
              Container(
                alignment: Alignment.center,
                height: 60,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 242, 243, 245),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Row(
                  children: [
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        '(디지털컨버전스)JAVA기반 스마트웹&앱 개발자 양성_A',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    SizedBox(width: 15)
                  ],
                ),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 7),
                    Text('훈련기관'),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              SizedBox(height: 5),
              Container(
                alignment: Alignment.centerLeft,
                height: 44,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 242, 243, 245),
                    borderRadius: BorderRadius.circular(8.0)),
                child: Row(children: [
                  SizedBox(width: 15),
                  Text(
                    '라인컴퓨터학원',
                    style: TextStyle(fontSize: 18),
                  ),
                ]),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 7),
                    Text('훈련일정'),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              SizedBox(height: 5),
              Container(
                alignment: Alignment.centerLeft,
                height: 44,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 242, 243, 245),
                    borderRadius: BorderRadius.circular(8.0)),
                child: Row(children: [
                  SizedBox(width: 15),
                  Text(
                    '2023.11.20 ~ 2024.05.14',
                    style: TextStyle(fontSize: 18),
                  ),
                ]),
              ),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 7),
                    Text('출석일'),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              SizedBox(height: 5),
              Container(
                alignment: Alignment.centerLeft,
                height: 44,
                width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                    color: Color.fromARGB(255, 242, 243, 245),
                    borderRadius: BorderRadius.circular(8.0)),
                child: Row(children: [
                  SizedBox(width: 15),
                  Text(
                    '75 / 119(일)',
                    style: TextStyle(fontSize: 18),
                  ),
                ]),
              ),
              Divider(height: 35),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 6),
                    Text(
                      '출석정보',
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width * 0.4,
                    margin: EdgeInsets.all(10),
                    child: DropdownButtonHideUnderline(
                      child: GFDropdown(
                        padding: const EdgeInsets.all(10),
                        borderRadius: BorderRadius.circular(5),
                        border:
                            const BorderSide(color: Colors.black12, width: 1),
                        dropdownButtonColor: Colors.white70,
                        value: yearDropdownValue,
                        // 현재 선택된 연도를 보여줌
                        onChanged: (dynamic newValue) {
                          setState(() {
                            yearDropdownValue = newValue.toString();
                          });
                        },
                        items: ['2024', '2023']
                            .map((value) => DropdownMenuItem(
                                  value: value,
                                  child: Text(value),
                                ))
                            .toList(),
                      ),
                    ),
                  ),
                  SizedBox(width: 10),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width * 0.4,
                    margin: EdgeInsets.all(10),
                    child: DropdownButtonHideUnderline(
                      child: GFDropdown(
                        padding: const EdgeInsets.all(10),
                        borderRadius: BorderRadius.circular(5),
                        border:
                            const BorderSide(color: Colors.black12, width: 1),
                        dropdownButtonColor: Colors.white70,
                        value: monthDropdownValue,
                        // 현재 선택된 월을 보여줌
                        onChanged: (dynamic newValue) {
                          setState(() {
                            monthDropdownValue = newValue.toString();
                          });
                        },
                        items: [
                          '01',
                          '02',
                          '03',
                          '04',
                          '05',
                          '06',
                          '07',
                          '08',
                          '09',
                          '10',
                          '11',
                          '12',
                        ]
                            .map((value) => DropdownMenuItem(
                                  value: value,
                                  child: Text(value),
                                ))
                            .toList(),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: ListView(
                  scrollDirection: Axis.vertical,
                  children: [
                    DataTable(
                      columns: const <DataColumn>[
                        DataColumn(
                          label: Expanded(
                            child: Text('날짜',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 16)),
                          ),
                        ),
                        DataColumn(
                          label: Expanded(
                            child: Text('출결정보',
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 16)),
                          ),
                        ),
                      ],
                      rows: const <DataRow>[
                        DataRow(cells: <DataCell>[
                          DataCell(Center(child: Text('03-13(수)'))),
                          DataCell(Center(child: Text('출석'))),
                        ]),
                        DataRow(cells: <DataCell>[
                          DataCell(Center(child: Text('03-12(화)'))),
                          DataCell(Center(child: Text('출석'))),
                        ]),
                        DataRow(cells: <DataCell>[
                          DataCell(Center(child: Text('03-11(월)'))),
                          DataCell(Center(child: Text('출석'))),
                        ]),
                        DataRow(cells: <DataCell>[
                          DataCell(Center(child: Text('03-08(금)'))),
                          DataCell(Center(child: Text('출석'))),
                        ]),
                        DataRow(cells: <DataCell>[
                          DataCell(Center(child: Text('03-07(목)'))),
                          DataCell(Center(child: Text('출석'))),
                        ]),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
// InputBorder.none!
