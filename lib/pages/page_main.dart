import 'package:flutter/material.dart';
import 'package:lms_student_app/pages/page_alarm.dart';
import 'package:lms_student_app/pages/page_home.dart';
import 'package:lms_student_app/pages/page_more.dart';
import 'package:lms_student_app/pages/page_timetable.dart';

class PageMain extends StatefulWidget {
  const PageMain({super.key});

  @override
  State<PageMain> createState() => _SimpleBottomNavigationState();
}

class _SimpleBottomNavigationState extends State<PageMain> {
  static const List _widgetOptions = <Widget>[
    PageHome(),
    PageTimeTable(),
    PageAlarm(),
    PageMore(),
  ];

  List<BottomNavigationBarItem> _icons = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home_outlined),
      activeIcon: Icon(Icons.home_rounded),
      label: 'Home',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.calendar_month_outlined),
      activeIcon: Icon(Icons.calendar_month_rounded),
      label: 'TIMETABLE',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.notifications),
      activeIcon: Icon(Icons.notifications),
      label: 'ALARM',
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.menu_outlined),
      activeIcon: Icon(Icons.menu_outlined),
      label: 'MORE',
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  int _selectedIndex = 0;

  BottomNavigationBarType _bottomNavType = BottomNavigationBarType.fixed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          selectedItemColor: Colors.black,
          unselectedItemColor: const Color(0xff757575),
          type: _bottomNavType,
          onTap: _onItemTapped,
          items: _icons),
    );
  }
}
