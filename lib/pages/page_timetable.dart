import 'package:flutter/material.dart';
import 'package:lms_student_app/pages/page_objection.dart';
import 'package:table_calendar/table_calendar.dart';

class PageTimeTable extends StatefulWidget {
  const PageTimeTable({Key? key}) : super(key: key);

  @override
  _PageTimeTableState createState() => _PageTimeTableState();
}

class _PageTimeTableState extends State<PageTimeTable> {
  DateTime? _selectedDateRangeStart; // 선택된 범위의 시작 날짜
  DateTime? _selectedDateRangeEnd; // 선택된 범위의 종료 날짜

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          '시간표',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 1.1,
        child: Column(
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //SizedBox(width: 0),
                  Container(
                    height: MediaQuery.of(context).size.width * 0.2,
                    width: MediaQuery.of(context).size.width * 0.46,
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        minimumSize: Size(100, 50),
                      ),
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(Icons.date_range, color: Colors.black),
                          SizedBox(width: 15),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '${_selectedDateRangeStart != null ? '${_selectedDateRangeStart!.year}-${_selectedDateRangeStart!.month}-${_selectedDateRangeStart!.day}' : '날짜 선택'}',
                                style: TextStyle(color: Colors.black),
                                textAlign: TextAlign.center,
                              ),
                              if (_selectedDateRangeEnd != null)
                                Text(
                                  '~ ${_selectedDateRangeEnd!.year}-${_selectedDateRangeEnd!.month}-${_selectedDateRangeEnd!.day}',
                                  style: TextStyle(color: Colors.black),
                                  textAlign: TextAlign.center,
                                ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(width: 15),
                  Container(
                    height: MediaQuery.of(context).size.width * 0.2,
                    width: MediaQuery.of(context).size.width * 0.46,
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.black),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        minimumSize: Size(1000, 500),
                      ),
                      onPressed: () {
                        // 다음 페이지로 이동
                        if (_selectedDateRangeStart != null) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => PageObjection(
                                selectedStartDate: _selectedDateRangeStart!,
                                selectedEndDate: _selectedDateRangeEnd ??
                                    _selectedDateRangeStart!,
                              ),
                            ),
                          );
                        } else {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text('날짜 선택 필요'),
                                content: Text('시작 날짜를 선택하세요.'),
                                actions: <Widget>[
                                  TextButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('확인'),
                                  ),
                                ],
                              );
                            },
                          );
                        }
                      },
                      child: Text('+ 출석 이의 신청',
                          style: TextStyle(color: Colors.black)),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  height: MediaQuery.of(context).size.height / 1.8,
                  decoration: BoxDecoration(
                    border: Border(top: BorderSide(color: Colors.black)),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(20),
                    ),
                  ),
                  padding: EdgeInsets.only(top: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.9,
                    child: SingleChildScrollView(
                      child: TableCalendar(
                        rangeStartDay: _selectedDateRangeStart,
                        rangeEndDay: _selectedDateRangeEnd,
                        rangeSelectionMode: RangeSelectionMode.enforced,
                        calendarFormat: CalendarFormat.month,
                        calendarStyle: CalendarStyle(),
                        locale: 'ko_KR',
                        onDaySelected: _onDaySelected,
                        selectedDayPredicate: (_) => false,
                        firstDay: DateTime.utc(2023),
                        lastDay: DateTime.utc(2100, 3, 14),
                        focusedDay: DateTime.now(),
                        headerStyle: HeaderStyle(
                          titleCentered: true,
                          formatButtonVisible: false,
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void _onDaySelected(DateTime day, DateTime focusedDay) {
    setState(() {
      if (_selectedDateRangeStart == null ||
          (_selectedDateRangeStart != null && _selectedDateRangeEnd != null)) {
        _selectedDateRangeStart = day;
        _selectedDateRangeEnd = null;
      } else {
        if (day.isAfter(_selectedDateRangeStart!)) {
          _selectedDateRangeEnd = day;
        } else {
          _selectedDateRangeEnd = _selectedDateRangeStart;
          _selectedDateRangeStart = day;
        }
      }
    });
  }
}
