import 'package:flutter/material.dart';
import 'package:lms_student_app/pages/page_alarm.dart';
import 'package:lms_student_app/pages/page_attendance.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 40),
            Container(
                width: MediaQuery.of(context).size.width / 1,
              //height: MediaQuery.of(context).size.width / 4,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //SizedBox(width: MediaQuery.of(context).size.width / 6),
                  Container(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.account_circle,
                      size: 80,
                    ),
                  ),
                  SizedBox(width: 20),
                  Text(
                    'Rechard',
                    style: TextStyle(fontSize: 25),
                  ),
                  SizedBox(width: 30)
                ],
              ),
            ),
            SizedBox(height: 30),
            Container(height: 2, color: Colors.black, child: Divider()),
            // 프로필 카드와 출결 현황 사이 간격
            SizedBox(height: 40),
            InkWell(
              // 랜더링 없는 부분도 클릭할 수 있게 해주는 소스
              //behavior: HitTestBehavior.opaque,
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageAttendance()),
                );
              },
              // 출결 현황 박스
              child: Container(
                width: MediaQuery.of(context).size.width / 1.1,
                height: 230,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]!),
                    borderRadius: BorderRadius.circular(10.0)),
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Text(
                      '출결 현황',
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 7),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        //SizedBox(width: MediaQuery.of(context).size.width * 0.0008),
                        //Spacer(flex: 20,),
                        Container(
                            alignment: Alignment.center,
                            height: 25,
                            width: 85,
                            decoration: BoxDecoration(
                                color: Color.fromARGB(255, 225, 227, 230),
                                borderRadius: BorderRadius.circular(8.0)),
                            child: Text('나의 출석률')),
                        SizedBox(width: 10),
                        Text('63.0% (75/119일)', textAlign: TextAlign.left,)
                      ],
                    ),
                    SizedBox(height: 15),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 15,
                      child: LinearProgressIndicator(
                        value: 0.3,
                        backgroundColor: Color.fromARGB(255, 225, 227, 230),
                        color: Colors.black45,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            Color.fromARGB(255, 38, 136, 235)),
                        minHeight: 10.0,
                        semanticsLabel: 'semanticsLabel',
                        semanticsValue: 'semanticsValue',
                      ),
                    ),
                    SizedBox(height: 15),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                '출석',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color.fromARGB(255, 0, 120, 212)),
                                value: 0.9,
                              ),
                              SizedBox(height: 10),
                              Text('75')
                            ],
                          ),
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                '지각',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color.fromARGB(255, 0, 120, 212)),
                                value: 0.9,
                              ),
                              SizedBox(height: 10),
                              Text('0')
                            ],
                          ),
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                '조퇴',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color.fromARGB(255, 0, 120, 212)),
                                value: 0.9,
                              ),
                              SizedBox(height: 10),
                              Text('0')
                            ],
                          ),
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                '외출',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color.fromARGB(255, 0, 120, 212)),
                                value: 0.9,
                              ),
                              SizedBox(height: 10),
                              Text('0')
                            ],
                          ),
                          SizedBox(width: 20),
                          Column(
                            children: [
                              Text(
                                '결석',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 10),
                              CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color.fromARGB(255, 0, 120, 212)),
                                value: 0.9,
                              ),
                              SizedBox(height: 10),
                              Text('1')
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 40),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageAlarm()),
                );
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 1.1,
                height: 85,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]!),
                    borderRadius: BorderRadius.circular(10.0)),
                padding: EdgeInsets.only(left: 12),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.fromLTRB(16.0, 10, 0, 0)),
                    Text(
                      '시험',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      '2024-03-11',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('UI-UX 구현 테스트')
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => PageAlarm()),
                );
              },
              child: Container(
                width: MediaQuery.of(context).size.width / 1.1,
                height: 85,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey[300]!),
                    borderRadius: BorderRadius.circular(10.0)),
                padding: EdgeInsets.only(left: 12),
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(padding: EdgeInsets.fromLTRB(16.0, 10, 0, 0)),
                    Text(
                      '시험',
                      style: TextStyle(fontSize: 16),
                    ),
                    Text(
                      '2024-03-18',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('GIT 사용법 숙지 테스트')
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
