import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class PageObjection extends StatefulWidget {
  //final DateTime selectedDate; // selectedDate 변수를 추가합니다.

  final DateTime selectedStartDate; // 선택된 시작 날짜 변수 추가
  final DateTime selectedEndDate; // 선택된 끝나는 날짜 변수 추가

  const PageObjection({
    Key? key,
    required this.selectedStartDate,
    required this.selectedEndDate,
  }) : super(key: key);

  @override
  State<PageObjection> createState() => _PageObjectionState();
}

class _PageObjectionState extends State<PageObjection> {
  final picker = ImagePicker();
  XFile? _image; // 카메라로 촬영한 이미지를 저장할 변수
  List<XFile?> multiImage = []; // 갤러리에서 여러 장의 사진을 선택해서 저장할 변수
  List<XFile?> images = []; // 가져온 사진들을 보여주기 위한 변수

  Future<void> getImage(ImageSource imageSource) async {
    final XFile? pickedFile = await picker.pickImage(source: imageSource);
    if (pickedFile != null) {
      setState(() {
        _image = XFile(pickedFile.path);
      });
    } else {
      // 이미지가 선택되지 않은 경우 사용자에게 알림
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('No image selected'),
            content: Text('Please select an image.'),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('OK'),
              ),
            ],
          );
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(top: 3),
          child: Text(
            '출석 이의 신청',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 60),
                    child: Text(
                      '시작 날짜',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    alignment: Alignment.centerLeft,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      onPressed: () {},
                      child: Text(
                        "${widget.selectedStartDate.toString().substring(0, 10)}", // 시작 날짜 표시
                        style: TextStyle(
                            color: Color.fromARGB(255, 109, 120, 133)),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(right: 50),
                    child: Text(
                      '끝나는 날짜',
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    alignment: Alignment.centerLeft,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)),
                      ),
                      onPressed: () {},
                      child: Text(
                        "${widget.selectedEndDate.toString().substring(0, 10)}", // 끝나는 날짜 표시
                        style: TextStyle(
                            color: Color.fromARGB(255, 109, 120, 133)),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          Column(
            children: [
              SizedBox(height: 20),
              Container(height: 1, color: Colors.grey, child: Divider()),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 20),
                    Text(
                      '사진 추가',
                      style:
                          TextStyle(color: Color.fromARGB(255, 109, 120, 133)),
                    ),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                child: Container(
                  decoration: BoxDecoration(color: Colors.grey),
                  padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  width: 130,
                  height: 130,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      _image != null
                          ? Image.file(File(_image!.path))
                          : Container(), // _image가 null이면 빈 컨테이너를 반환
                      Icon(
                        Icons.camera_alt,
                        color: Colors.white,
                        size: 50,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(width: 30),
              // 카메라로 촬영하기
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 0.5,
                              blurRadius: 5)
                        ],
                      ),
                      child: IconButton(
                          onPressed: () async {
                            _image = await picker.pickImage(
                                source: ImageSource.camera);
                            //카메라로 촬영하지 않고 뒤로가기 버튼을 누를 경우, null값이 저장되므로 if문을 통해 null이 아닐 경우에만 images변수로 저장하도록 합니다
                            if (_image != null) {
                              setState(() {
                                images.add(_image);
                              });
                            }
                          },
                          icon: Icon(
                            Icons.add_a_photo,
                            size: 30,
                            color: Colors.white,
                          ))),
                  // 갤러리에서 가져오기
                  Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.lightBlueAccent,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 0.5,
                              blurRadius: 5)
                        ],
                      ),
                      child: IconButton(
                          onPressed: () async {
                            multiImage = await picker.pickMultiImage();
                            setState(() {
                              //multiImage를 통해 갤러리에서 가지고 온 사진들은 리스트 변수에 저장되므로 addAll()을 사용해서 images와 multiImage 리스트를 합쳐줍니다.
                              images.addAll(multiImage);
                            });
                          },
                          icon: Icon(
                            Icons.add_photo_alternate_outlined,
                            size: 30,
                            color: Colors.white,
                          ))),
                ],
              ),
              SizedBox(height: 20),
              Container(height: 1, color: Colors.grey, child: Divider()),
              SizedBox(height: 10),
              Container(
                child: Row(
                  children: [
                    SizedBox(width: 20),
                    Text(
                      '출석 이의 신청 사유',
                      style:
                          TextStyle(color: Color.fromARGB(255, 109, 120, 133)),
                    ),
                    SizedBox(width: 100)
                  ],
                ),
              ),
              SizedBox(height: 10),
              SingleChildScrollView(
                child: Container(
                  width: 328,
                  height: 170,
                  decoration: BoxDecoration(
                      color: Color.fromARGB(255, 242, 243, 245),
                      borderRadius: BorderRadius.circular(8.0)),
                  child: TextField(
                    style: TextStyle(fontSize: 17),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: '출석 이의 신청 사유를 입력해주세요.',
                      contentPadding: EdgeInsets.fromLTRB(10, 15, 0, 0),
                    ),
                    maxLines: 10,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 55, // 버튼의 높이를 설정합니다.
                      alignment: Alignment.center,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color.fromARGB(255, 45, 129, 224),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          minimumSize: Size(100, 50),
                        ),
                        onPressed: () {},
                        child:
                            Text('저장', style: TextStyle(color: Colors.white)),
                      ),
                    ),
                    SizedBox(width: 20),
                    Container(
                      height: 55, // 버튼의 높이를 설정합니다.
                      alignment: Alignment.center,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.white70,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          minimumSize: Size(100, 50),
                        ),
                        onPressed: () {},
                        child:
                            Text('취소', style: TextStyle(color: Colors.black)),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ]),
      ),
    );
  }
}
