import 'package:flutter/material.dart';

class PageAlarm extends StatefulWidget {
  const PageAlarm({super.key});

  @override
  State<PageAlarm> createState() => _PageAlarmState();
}

class _PageAlarmState extends State<PageAlarm> {
  List<String> items = ['Item 0', 'Item 1', 'Item 2', 'Item 3', 'Item 4'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Padding(
          padding: EdgeInsets.only(top: 3),
          child: Text(
            '알림',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () {
                  // 모든 요소 삭제
                  setState(() {
                    items.clear();
                  });
                },
                child: Text(
                  '모두 지우기',
                  style: TextStyle(color: Colors.blue),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: Icon(Icons.notifications_none),
                  iconColor: Colors.blueAccent,
                  title: Text(items[index]),
                  trailing: TextButton(
                    onPressed: () {
                      setState(() {
                        items.removeAt(index);
                      });
                    },
                    child: Text(
                      "삭제",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
